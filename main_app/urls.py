from django.contrib import admin
from django.contrib.auth import views as auth_views
from django.urls import path, include
from django.conf import settings
from django.conf.urls.static import static
from companies_app import views as companies_views
from main_app import views as main_views

app_name = 'main_app'
urlpatterns = [          
    path('',main_views.main, name = 'main'),
    # path('<int:company_id>/',companies_views.etrafli, name = 'etrafli'),
]