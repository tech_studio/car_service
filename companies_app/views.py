from django.shortcuts import render,get_object_or_404
from companies_app.models import Company
from calendar_app.views import get_date,prev_month,next_month
from datetime import datetime, timedelta, date
from django.shortcuts import render, get_object_or_404
from django.http import HttpResponse, HttpResponseRedirect
from django.views import generic
from django.urls import reverse
from django.utils.safestring import mark_safe
import calendar
from calendar_app.utils import Calendar
from calendar_app.forms import EventForm

# Create your views here.
def etrafli(request,company_id):

    context = {}
    company  = get_object_or_404(Company, company_id = company_id)
    context["company"] = company
    item = company
    # ''' device locakitino''''
    result = []
    
    new_data = Company.objects.get(
    company_id=item.company_id)
    result.append({
        "company_id": item.company_id,
        "coords": {"lat": item.company_latitude, "lng": item.company_longitude},
        "text": "Cihaz no: {} ".format(item.company_id),
        "company_name": "Servis name: {} ".format(item.company_name),
                
    })
    # except Company.DoesNotExist:
    #     pass
    context["object_list"] = result
    
    d = get_date(request.GET.get('month', None))
    cal = Calendar(d.year, d.month)
    html_cal = cal.formatmonth(withyear=True,comp_id = company_id)
    context['calendar'] = mark_safe(html_cal)
    context['prev_month'] = prev_month(d)
    context['next_month'] = next_month(d)
    return render(request, "companies_app/company.html", context)

