from django.contrib import admin
from .models import *
# Register your models here.

admin.site.register([Services])
@admin.register(Company)
class ServicesAdmin(admin.ModelAdmin):
    list_display = ['company_id', 'company_name','adress']
