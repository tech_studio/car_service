from django.contrib import admin
from django.contrib.auth import views as auth_views
from django.urls import path, include
from django.conf import settings
from django.conf.urls.static import static
from companies_app import views as companies_views

app_name = 'companies_app'
urlpatterns = [   
    
    path('<int:company_id>/',companies_views.etrafli, name = 'etrafli'),
]