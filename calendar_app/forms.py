from django.forms import ModelForm, DateInput
from calendar_app.models import Event

class EventForm(ModelForm):
  class Meta:
    model = Event
    # datetime-local is a HTML5 input type, format to make date time show on fields
    widgets = {
      'start_time': DateInput(attrs={'type': 'datetime-local'}, format='%Y-%m-%d %H:%M:%S'),
      'end_time': DateInput(attrs={'type': 'datetime-local'}, format='%Y-%m-%d %H:%M:%S'),
    }
    fields = '__all__'

  def __init__(self, *args, **kwargs):
    super(EventForm, self).__init__(*args, **kwargs)
    # input_formats parses HTML5 datetime-local input to datetime field
    self.fields['start_time'].input_formats = ('%Y-%m-%d %H:%M:%S',)
    self.fields['end_time'].input_formats = ('%Y-%m-%d %H:%M:%S',)
