from django.db import models
from companies_app.models import Company
# Create your models here.


from django.urls import reverse

class Event(models.Model):
    company = models.ForeignKey(Company, on_delete=models.CASCADE)
    title = models.CharField(max_length=200)
    description = models.TextField()
    start_time = models.DateTimeField()
    end_time = models.DateTimeField()
    

    @property
    def get_html_url(self):
        url = reverse('calendar_app:event_edit', args=(self.id,))
        return f'<a href="{url}"> {self.title} </a>'
